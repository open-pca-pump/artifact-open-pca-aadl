# Open PCA Pump AADL Model with ISO14971 Properties

Repository for the Open PCA Pump AADL Models and the risk analysis artifacts

* Clone the repository and import the following projects into OSATE 
* Functional_PCA, Medical_Device_Platform, bless-props and physical.  
* Install [Awas plugin](https://awas.sireum.org/doc/01-getting-started/index.html) for OSATE 