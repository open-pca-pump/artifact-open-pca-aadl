--Functional.java

--logical (functional) architecture

package Functional
public

with
  Base_Types,   --AADL data model
  FG,           --feature group definitions
  Operation,    --operation subsystem
  Comm,         --communication subsystem
  Human_Machine_Interface,  --graphical user interface (a.k.a. control panel)
--  Services,     --platform services
  Peripherals,
  Abstract_Devices,
--  Buses,
  Security,
  Safety            --safety subsystem
  ;

flag renames data Base_Types::Boolean;

--functional architecture for a generic medical device using the Platform platform
system Generic_Medical_Function
	features
	  --external connection to Ethernet, WiFi, etc.
--    ethernet : requires bus access Buses::CAT5;
    --physical jack to connect to maintenance controller
--	jack : requires bus access  Buses::Maintenance_Bus;  
	sense: feature group FG::Sensing;  --physical input to device
	act : feature group FG::Actuation;  --what the device does
	alarm : feature group FG::Alarm_Indication;	 --alarm to announce 
	ui : feature group FG::User_Interface;
end Generic_Medical_Function;

system implementation Generic_Medical_Function.i
	subcomponents
		sensors_actuators: system Peripherals::Sensors_Actuators.i;
		safety_subsystem: system Safety::Safety.i;
		power_subsystem: system Power.i; -- {Actual_Function_Binding => (classifier(Hardware::PCB.power));};
		operation_subsystem: system Operation::Operation.i;
		security_subsystem: system Security::Security_System.i; --with software crypto/hash
		communication_subsystem: system Comm::Communications.i;
		gui_subsystem: system Human_Machine_Interface::HMI.i;
	connections
		-- GUI-human
		ui_t: port ui.ui_touch -> gui_subsystem.ui_touch;
		ui_s: port gui_subsystem.ui_sound -> ui.ui_sound;
		ui_img: port gui_subsystem.ui_image -> ui.ui_image;
		--alarm used to change operation
		oa: feature group safety_subsystem.alarm -> operation_subsystem.alarm;
		--safety architecture stop function
		ha: port safety_subsystem.halt -> sensors_actuators.halt;
		--report self-detected malfunction
		mal: port sensors_actuators.malfunction -> safety_subsystem.peripheral_malfunction;
		--  safety-peripherals
		s_p: feature group safety_subsystem.to_peripherals -> sensors_actuators.from_safety;
		ps: feature group sensors_actuators.to_safety -> safety_subsystem.from_peripherals;
		--  operation-peripherals
-- brl 5/17/2020
		o_sa: feature group operation_subsystem.to_peripherals -> sensors_actuators.from_operation;
		sa_o: feature group sensors_actuators.to_operation -> operation_subsystem.from_peripherals;
		-- operation-safety
-- brl 5/17/2020
		o_s: feature group operation_subsystem.to_safety -> safety_subsystem.from_operation;
		s_o: feature group safety_subsystem.to_operation -> operation_subsystem.from_safety;
		-- operation-GUI
		o_g: feature group operation_subsystem.to_user -> gui_subsystem.from_operation;
		g_o: feature group gui_subsystem.to_operation -> operation_subsystem.from_user;
		--  safety-GUI
		--alarm displayed on user interface
		ua: feature group safety_subsystem.alarm -> gui_subsystem.alarm;
		inac: port gui_subsystem.alarm_inactivation -> safety_subsystem.alarm_inactivation;

		--  power-safety
		p_s: feature group power_subsystem.to_safety -> safety_subsystem.from_power;
		--  power-operation
		p_o: feature group power_subsystem.to_operation -> operation_subsystem.from_power;
		--actuators control enviornment
		ac: feature group sensors_actuators.act -> act;
		--sensors perceive environment
		sn: feature group sense -> sensors_actuators.sense;
--	  -- network connection
--	  nc : bus access ethernet <-> network_controller.external;
--	  -- maintenance jack
--	  mj : bus access jack <-> maintenance_controller.jack;
--		m_j: bus access jack -> communication_subsystem.jack;
		cryp_o_sec: feature group operation_subsystem.crypto_control -> security_subsystem.crypto_control;
		cryp_sec_o: feature group security_subsystem.crypto_response -> operation_subsystem.crypto_response;
		comm_o_c: feature group operation_subsystem.to_comm -> communication_subsystem.to_comm;
		comm_c_o: feature group communication_subsystem.from_comm -> operation_subsystem.from_comm;
		--maintenance functions
		mo_t_c: feature group operation_subsystem.to_maintenance -> communication_subsystem.to_maintenance;
		mc_f_o: feature group communication_subsystem.from_maintenance -> operation_subsystem.from_maintenance;
		sa_c: feature group safety_subsystem.alarm -> communication_subsystem.alarm;
		tfl: port safety_subsystem.the_fault_log -> communication_subsystem.the_fault_log;
		gfl: port communication_subsystem.get_fault_log -> safety_subsystem.get_fault_log;
end Generic_Medical_Function.i;


--added peripherals to perform medical function 
--this is where the sensors and actuators go
--system Peripherals
--	features
--	  halt : in event port;  --halt medical device operation on safety command 
--    malfunction : out data port flag;  --hardware detected malfunction, such as pump overheating
--    from_safety : feature group inverse of FG::Safety_Peripherals;
--    from_operation : feature group inverse of FG::Operation_Peripherals;
--end Peripherals;

--power subsystem
	system Power extends Abstract_Devices::Abstract_Power
	features
	  to_safety: feature group FG::Power_to_Safety;        --signals to safety subsystem
	  to_operation: feature group FG::Power_to_Operation;  --signals to operation subsystem
	end Power;

  system implementation Power.i
  	
  end Power.i;

end Functional;