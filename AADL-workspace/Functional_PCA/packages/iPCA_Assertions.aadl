--iPCA_Assertions.aadl

package iPCA_Assertions
  public
    with iPCA_Properties;
   
annex Assertion 
{**
ghost variables
  def INFUSION_RATE~quantity mlph         --actual rate of drug infusion into patient, must be within tolerance of commanded and measured rates
  def FAULT_LOG~fault_log             --contents of fault log held by fault_logger
  def EVENT_LOG~event_log             --contents of event log held by event_logger
  def DRUG_LIBRARY~drug_library          --contents of drug library held by drug_library_thread
  def DRUG_CODE~quantity whole
  def POST_FAIL~boolean             --power-on self test failure detected
  def RAM_FAIL~boolean              --random-access memory failure detected
  def ROM_FAIL~boolean              --read-only memory failure detected
  def CPU_FAIL~boolean              --processor failure detected
  def TM_FAIL~boolean               --thread monitor failure detected
--  def BTTY_FAIL~boolean             --battery failure
  def PS_FAIL~boolean               --power supply failure
--  def VOLTAGE_OOR~boolean               --voltage out-of-range
  def BASAL_OVER_INFUSION~boolean   --infusion greater than basal rate+tolerance detected during basal infusion
  def BOLUS_OVER_INFUSION~boolean   --infusion greater than bolus rate+tolerance detected during bolus infusion
  def SQUARE_OVER_INFUSION~boolean  --infusion greater than square bolus rate+tolerance detected during square bolus infusion
  def AIR_IN_LINE~boolean           --bubble detected
  def EMPTY_RESERVOIR~boolean       --reservoir contents less than minimum
  def PUMP_OVERHEATED~boolean       --pump is above allowed temperature
  def DOWNSTREAM_OCCLUSION~boolean  --blockage between pump and patient detected
  def UPSTREAM_OCCLUSION~boolean    --blockage between reservoir and pump detected
  def MAX_DOSE_INFUSED~boolean      --maximum dose reached
  def BASAL_UNDER_INFUSION~boolean  --infusion less than basal rate detected-tolerance during basal infusion
  def BOLUS_UNDER_INFUSION~boolean  --infusion less than bolus rate detected-tolerance during bolus infusion
  def SQUARE_UNDER_INFUSION~boolean --infusion less than square bolus rate-tolerance detected during square bolus infusion
  def BTTY_BACKUP~boolean           --operating on battery
  def LOW_RESERVOIR~boolean         --reservoir contents less than warning threshold
--  def LOW_BATTERY~boolean           --battery energy less than warning threshold
  def LONG_PAUSE~boolean            --long pause in user input
  def ALARM_INACTIVATION~boolean    --value held within control panel about whether alarm are inactivated
  def APP_CONNECTED~boolean         --control app is controlling this PCA pump
  def PRESCRIBED_BASAL_RATE~quantity mlph --intended rate for basal infusion
  def PRESCRIBED_BOLUS_RATE~quantity mlph --intended rate for bolus infusion
  def PRESCRIBED_KVO_RATE~quantity mlph   --intended rate for keep-vein-open infusion
  def PRESCRIBED_VTBI~quantity ml       --intended volume to be infused (myRx.vtbi in Prescription_Checker.i)
  def SQUARE_BOLUS_DURATION~quantity min --time for infusion of clinician-requested bolus
  def XXXX~boolean                  --dummy placeholder
  def BATTERY_VOLTAGE~quantity mV		  --battery voltage from the battery to the power system
  def BATTERY_CURRENT~quantity uA		  --battery current from the battery to the power system
  def REMAINING_BATTERY_TIME~quantity min  --time left to run on battery backup
  def NO_ALARM~boolean
  def NO_WARNING~boolean
  def SOFT_LIMIT~boolean
  def INPUT_NEEDED~boolean
  def DRUG_NOT_IN_LIBRARY~boolean
  def HARD_LIMIT_VIOLATED~boolean
  def PRIME_FAIL~boolean
  def MINIMUM_TIME_BETWEEN_BOLUS~quantity min
  def PATIENT_BUTTON_REQUEST~boolean
  def LAST_PATIENT_BOLUS~time
  def RxOKAY~boolean  --equivalent to RxApproved() in Prescription_Checker.i
  def PATIENT_BOLUS_RATE~quantity mlph
  def SQUARE_BOLUS_RATE~quantity mlph
  def BASAL_RATE~quantity mlph
  def LAST_ACTION_A~last_action
  def CLINICIAN_APPROVED_SOFT_LIMIT~boolean
  def MAX_DRUG_PER_HOUR~quantity ml
  def NEW_EVENT_RECORD~event_record
  def POWER_VOLTAGE~quantity V
  def PATIENT_PRESCRIPTION~prescription_record  --prescription read from drug label
   
<< NDF: k~quantity whole : not (exists j~quantity whole --there is NO entry 
         in 0 .. (k - 1)  --in the array of drug library
         that (DRUG_LIBRARY[j].code = DRUG_CODE) )>>      
 	 
<< ALERT_STOP_START: : false>>  --R5.5.0(18) alert-stop-start sequence
  --alarm requiring immediate clinician attention
<< IMMEDIATE_ALARM: : (BASAL_OVER_INFUSION or BOLUS_OVER_INFUSION or SQUARE_OVER_INFUSION)^(-1) >>
  --alarm requiring prompt clinician attention
<< PROMPT_ALARM: : (ALERT_STOP_START() or AIR_IN_LINE or EMPTY_RESERVOIR or PUMP_OVERHEATED
	or DOWNSTREAM_OCCLUSION or UPSTREAM_OCCLUSION)^(-1) >>
  --warning requiring clinician attention, sometime
<< DELAYED_ALARM: : (BTTY_BACKUP or LOW_RESERVOIR or LOW_BATTERY() or LONG_PAUSE
    or VOLTAGE_OOR() or MAX_DOSE_INFUSED or BASAL_UNDER_INFUSION or BOLUS_UNDER_INFUSION or SQUARE_UNDER_INFUSION)^(-1) >>
  --hardware-detected failure, processor and threads may not be functioning
<< HW_FAIL: : POST_FAIL or RAM_FAIL or ROM_FAIL or CPU_FAIL or TM_FAIL or BTTY_FAIL() 
    or  PS_FAIL or VOLTAGE_OOR() >>
  --alarm type sent to network manager
<< INFUSION_CLASS: s~status +=>  
  Stopped->INFUSION_RATE = 0.0 mlph,
  Bolus->XXXX,  --INFUSION_RATE in 
  Basal->XXXX,
  KVO->XXXX,
  Square_Bolus->XXXX>>
  --specifies meaning of each alarm type
<< ALARM_TYPE: a~alarms +=> --has enumeration value of first element when predicate in 2nd element is true
  Pump_Overheated->PUMP_OVERHEATED,  
  Defective_Battery->BTTY_FAIL(),
  Low_Battery->LOW_BATTERY(),
  POST_Failure->POST_FAIL, 
  RAM_Failure->RAM_FAIL, 
  ROM_failure->ROM_FAIL, 
  CPU_Failure->CPU_FAIL,
  Thread_Monitor_Failure->TM_FAIL,
  Air_In_Line->AIR_IN_LINE, 
  upstream_occlusion->UPSTREAM_OCCLUSION, 
  downstream_occlusion->DOWNSTREAM_OCCLUSION,
  Empty_Reservoir->EMPTY_RESERVOIR, 
  basal_overinfusion->BASAL_OVER_INFUSION, 
  bolus_overinfusion->BOLUS_OVER_INFUSION, 
  square_bolus_overinfusion->SQUARE_OVER_INFUSION,
  No_Alarm->NO_ALARM >>    

--specifies meaning of each warning type
<< WARNING_TYPE: w~warnings +=>
  at_max_drug_per_hour->MAX_DOSE_INFUSED,
--  Below_VTBI_Soft_Limit->BELOW_VTBI_SOFT,
--  Above_VTBI_Soft_Limit->ABOVE_VTBI_SOFT,
--  Below_Basal_Rate_Soft_Limit->BELOW_BASAL_SOFT,
--  Above_Basal_Rate_Soft_Limit->ABOVE_BASAL_SOFT,
  Soft_Limit->SOFT_LIMIT,
  Low_Reservoir->LOW_RESERVOIR,
  basal_underinfusion->BASAL_UNDER_INFUSION,
  bolus_underinfusion->BOLUS_UNDER_INFUSION,
  square_bolus_underinfusion->SQUARE_UNDER_INFUSION,
  Input_Needed->INPUT_NEEDED,
  Long_Pause->LONG_PAUSE,
  drug_not_in_library->DRUG_NOT_IN_LIBRARY,
  hard_limit_violated->HARD_LIMIT_VIOLATED,
  voltage_oor->VOLTAGE_OOR(),
  Priming_Failure->PRIME_FAIL,
  No_Warning->NO_WARNING >> 

-- conditions for stopping the pump  
<< STOP_PUMP: :
  PUMP_OVERHEATED or BTTY_FAIL() or  TM_FAIL or AIR_IN_LINE
  or UPSTREAM_OCCLUSION or DOWNSTREAM_OCCLUSION or EMPTY_RESERVOIR >>  

--conditions for pumping at KVO rate
<< PUMP_KVO: :
  BASAL_OVER_INFUSION or BOLUS_OVER_INFUSION or SQUARE_OVER_INFUSION or MAX_DOSE_INFUSED
  or LOW_BATTERY()
>>  

<< PATIENT_REQUEST_NOT_TOO_SOON:x~time:
  PATIENT_BUTTON_REQUEST@x and not (exists t~time in (x-MINIMUM_TIME_BETWEEN_BOLUS),,x
    that PATIENT_REQUEST_NOT_TOO_SOON(t))
>>

<< PATIENT_REQUEST_TOO_SOON:x~time:
  PATIENT_BUTTON_REQUEST@x and PATIENT_REQUEST_NOT_TOO_SOON(LAST_PATIENT_BOLUS)
  and (x-MINIMUM_TIME_BETWEEN_BOLUS) <= LAST_PATIENT_BOLUS
>>
   
 --the infusion flow rate shall be:
--  =0, after stop pump completely (from safety architecture), 
--      clinician pressed stop button
--  =KVO rate, after KVO rate command, or
--      exceeded max drug per hour
--      some alarms,
--  =max rate, during patient-requested infusion
--  =square bolus rate, during clinician-commanded infusion	
--  =priming rate during pump priming
--  =basal rate, otherwise
--  "la" is a variable withing Rate_Controller
<< HALT : :(LAST_ACTION_A = last_action'SafetyStopPump) or (LAST_ACTION_A = last_action'StopButton) or (LAST_ACTION_A = last_action'EndPriming)>>  --pump at 0 if stop button, or safety architecture says, or done priming
<< KVO_RATE : :(LAST_ACTION_A = last_action'KVOcommand) or (LAST_ACTION_A = last_action'KVOalarm) or (LAST_ACTION_A = last_action'TooMuchJuice)>>  --pump at KVO rate when commanded, some alarms, or excedded hourly limit
<< PB_RATE : :LAST_ACTION_A = last_action'PatientButton>>  --patient button pressed, and allowed
<< CCB_RATE : :(LAST_ACTION_A = last_action'StartSquareBolus) or (LAST_ACTION_A = last_action'ResumeSquareBolus)>>  --clinician-commanded bolus start or resumption after patient bolus
<< PRIME_RATE : :LAST_ACTION_A = last_action'StartPriming>>  --priming pump
<< BASAL_RATE_A : :(LAST_ACTION_A = last_action'StartButton) or (LAST_ACTION_A = last_action'ResumeBasal) or (LAST_ACTION_A = last_action'SquareBolusDone)>>  --regular infusion
<< PUMP_RATE : returns quantity mlph := 
  [
  ( HALT() )-> 0.0 mlph,                                 			--no flow
  ( KVO_RATE() )-> #iPCA_Properties::KVO_Rate,      			--KVO rate
  ( PB_RATE() )-> PATIENT_BOLUS_RATE,							--maximum infusion upon patient request
  ( CCB_RATE() )-> SQUARE_BOLUS_RATE,             			--square bolus rate=vtbi/duration, from data port
  ( PRIME_RATE() )-> #iPCA_Properties::Prime_Rate,  			--pump priming
  ( BASAL_RATE_A() )-> BASAL_RATE                   			--basal rate, from data port
  ]
>>

--and def Rx_APPROVED           --placeholder for predicate declaring acceptable formulas
--<< Rx_APPROVED: : true >>  

<< LAST_A : : false >>

--the drug library has a drug record having specified drug code
<< DRUG_RECORD_HAS_CODE: drugRecord~drug_record, drugCode~drug_code : drugRecord.code = drugCode >>

<< LOW_BATTERY: :BATTERY_VOLTAGE < #iPCA_Properties::Low_Battery_Voltage V >>

<< BTTY_FAIL: :BATTERY_VOLTAGE < #iPCA_Properties::Defective_Battery_Voltage V >>
 
<< VOLTAGE_OOR: :(POWER_VOLTAGE < #iPCA_Properties::Minimum_Power_Voltage V) or (POWER_VOLTAGE > #iPCA_Properties::Maximum_Power_Voltage V) >>  

<< DLS : returns quantity whole := iPCA_Properties::Drug_Library_Size >>

  --the total drug in previous hour is at (or above) its limit
  <<PATIENT_AT_MAX_DRUG_PER_HOUR: : TOTAL_DRUG_IN_LAST_HOUR() >= MAX_DRUG_PER_HOUR >>  

  --the total drug in previous hour is at least 95% of its limit
  <<PATIENT_NEAR_MAX_DRUG_PER_HOUR: : TOTAL_DRUG_IN_LAST_HOUR() >= MAX_DRUG_PER_HOUR * iPCA_Properties::Near_Max_Drug_Per_Hour_Factor>>  --TODO

  -- sum of deleivered infusion rate for each second in previous hour
  -- note this assumes that the Period of the thread is 1 s, so there are 3600 samples in an hour
  -- also that INFUSION_RATE has units of mlph; without the magic of BLESS units you would need to convert it to ml/s in your code
  <<TOTAL_DRUG_IN_LAST_HOUR: returns quantity ml :=  
    (sum i~quantity whole in 1 .. iPCA_Properties::Samples_Per_Hour of INFUSION_RATE^(-i) * 1.0 s) >>  

**};

end iPCA_Assertions;