property set HazardousSituations is
	with ISO14971_80001, Hazards, Harms, ContributingFactors;
	
	OverInfusion : constant ISO14971_80001::Hazardous_Situation => [
		ID => "OverInfusion";
		Description => "Infusing drug when the patient's health is deteriorating";
		Hazard => Hazards::Haz1;
		Paths_to_Harm => ([
			Harm => Harms::H1;
			Contributing_Factors => (ContributingFactors::HealthDeteriorating); --fill
			Probability_of_Transition => Remote;
		]);
		Risk => High;
		Probability => Remote;
	];
	
	UnderInfusion : constant ISO14971_80001::Hazardous_Situation => [
		ID => "UnderInfusion";
		Description => "Not infusing adequate amount of drug when the patient's vitals are in good health";
		Hazard => Hazards::Haz2;
		Paths_to_Harm => ([
			Harm => Harms::H2;
			Contributing_Factors => (ContributingFactors::InGoodHealth);
			Probability_of_Transition => Probable;
		]);
		Risk => Low;
		Probability => Occasional;
	];
	
	IncorrectDrug : constant ISO14971_80001::Hazardous_Situation => [
		ID => "IncorrectDrug";
		Description => "Infusing incorrect drug into the patient leading to harming the patient";
		Hazard => Hazards::Haz3;
		Paths_to_Harm => ([
			Harm => Harms::H3;
			Contributing_Factors => (ContributingFactors::FailedToCheck);
			Probability_of_Transition => Probable;
		]);
		Risk => High;
		Probability => Remote;
	];
	
end HazardousSituations;