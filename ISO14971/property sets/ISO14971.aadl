property set ISO14971 is
with EMV2;
-- Severity labels from ISO14971 Table D.3
Catastrophic : constant EMV2::SeverityRange => 1;  --Results in patient death
Critical     : constant EMV2::SeverityRange => 2; -- Results in permanent impairment or life-threatening injury
Serious      : constant EMV2::SeverityRange => 3;  --Results in injury or impairment requiring professional medical intervention
Minor        : constant EMV2::SeverityRange => 4;  --Results in temporary injury or impairment not requiring professional medical intervention
Negligible   : constant EMV2::SeverityRange => 5;  --Inconvenience or temporary discomfort
NoEffect     : constant EMV2::SeverityRange => 5;  --same as Negligible because EMV2::SeverityRange = [1..5]

-- Likelihood labels from ISO14971 Table D.4
Frequent   : constant EMV2::LikelihoodLabels => A;  --  ≥ 10^-3
Probable   : constant EMV2::LikelihoodLabels => B;  --  < 10^−3 and ≥ 10^−4
Occasional : constant EMV2::LikelihoodLabels => C; -- < 10^−4 and ≥ 10^−5
Remote     : constant EMV2::LikelihoodLabels => D;  --  < 10^−5 and ≥ 10^−6
Improbable : constant EMV2::LikelihoodLabels => E;  --  < 10^−6


SeverityClassification: type enumeration (Catastrophic, Critical, Serious, Minor, Negligible, NoEffect);

ProbabilityLabels: type enumeration (Frequent, Probable, Occasional, Remote, Improbable);

ProbabilityBasis: type enumeration (HazardsPerHour, NumberOfOccurrencesPerHazard);

Hazards: list of record
  (
   CrossReference : aadlstring;   -- cross reference to an external document 
   HazardTitle : aadlstring;      -- short descriptive phrase for hazard
   Description : aadlstring;      -- description of the hazard (same as HazardTitle)
   Failure : aadlstring;          -- system deviation resulting in failure effect
   FailureEffect : aadlstring;    -- description of the effect of a failure (mode)
   Phases : list of aadlstring;   -- operational phases in which the hazard is relevant
   Environment : aadlstring;      -- description of operational environment
   Mishap : aadlstring;           -- description of event (series) resulting in 
                                  -- unintentional death, etc.(MILSTD882)
   Risk : aadlstring;             -- description of risk. Risk is characterized by 
                                  -- severity, likelihood, and occurrence probability
   FailureCondition : aadlstring; -- description of event (series) resulting in 
                                  -- unintentional death, etc.(ISP14971)
   Basis :  ISO14971::ProbabilityBasis;  --hazards per hour, or hazard per number of occurrences
   NumberOfOccurrencesPerHazard : aadlinteger;  --how many occurrences are expected to produce one hazard?                              
   Severity   : EMV2::SeverityRange; -- actual risk as severity
   QualitativeProbability           : ISO14971::ProbabilityLabels;      -- actual risk as probability
   QuantitativeProbability          : EMV2::ProbabilityRange;           -- probability of a hazard
     --if by number of occurrences = 1/NumberOfOccurrencesPerHazard
     --o.w. hazards per hour of operation   
   QualitativeProbabilityObjective  : ISO14971::ProbabilityLabels;      -- acceptable risk as probability 
   QuantitativeProbabilityObjective : EMV2::ProbabilityRange;           -- probability objective for hazard   
--   DevelopmentAssuranceLevel        : EMV2::DALLabels; -- level of rigor in development 
   VerificationMethod : aadlstring; -- verification method to address the hazard
   SafetyReport : aadlstring;       -- analysis/assessment of hazard
   Comment : aadlstring;            -- additional information about the hazard
   )
     applies to ({emv2}**error type, {emv2}**type set, {emv2}**error behavior state,
                 {emv2}**error source, {emv2}**error propagation, {emv2}**error event, {emv2}**error flow);

	
end ISO14971;